# Horizon Press

Welcome to Horizon Press, your go-to source for a diverse spectrum of news! As a
cutting-edge media organization, we specialize in delivering comprehensive coverage
ranging from global politics to the latest in sports and entertainment. At Horizon
Press, we strive to keep you informed and entertained with insightful articles that
cater to every facet of your interests.


## Installation
Take a clone from 'main' branch. 
After cloning, please use below commands to install project in your machine
Create Environment
```
python -m venv venv
```

Activate Environment
```
source venv/bin/activate     <-- On linux
.\venv\Scripts\activate      <-- On Windows
```

To install requirements
```
pip install -r requirements.txt
```

To migrate 
```
python manage.py migrate
```

Use below commands to load default categories into Categories table
```
python manage.py loaddata default_categories.json 
```

To runserver 
```
python manage.py runserver
```
