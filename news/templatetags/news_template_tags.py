from news.models import Categories

from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def load_categories_nav(context):
    context.update({"categories_navigations": Categories.objects.values("slug", "name")})
