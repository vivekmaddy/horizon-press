from django.contrib import messages
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from common.utils import get_sentence_sentiment, handle_news_export
from .models import News, Categories


# Create your views here.
def horizon_press_home(request, slug=None):
    context = {
        "news": None,
        "category_selected": None,
    }
    news = News.objects.order_by("-id")

    if slug:
        category = Categories.objects.get(slug=slug)
        news = news.filter(category=category).order_by("-id")
        context["category_selected"] = category

    paginator = Paginator(news, 6)  # Show 25 contacts per page.

    page_number = request.GET.get("page", 1)
    paginated_news = paginator.get_page(page_number)
    context["news"] = paginated_news

    return render(request, "news/news.html", context)


def submit_news(request, slug):
    try:
        if request.method == "POST":
            if Categories.objects.count() < 5:
                raise Exception("Minimum 5 Categories required!")
            sentence = request.POST.get("sentence", None)
            if sentence:
                category = Categories.objects.get(slug=slug)
                result, pred, err_msg = get_sentence_sentiment(sentence)
                if result and pred:
                    created = News.objects.create(
                        category=category,
                        sentence=sentence,
                        label=pred["label"].lower(),
                        score=pred["score"],
                        transformers_result_dict=pred
                    )
                    if created:
                        messages.success(request, "Created!")
                    else:
                        raise Exception("Failed!")
                else:
                    raise Exception(err_msg if err_msg else "unable to find sentiment!")

    except Exception as err:
        messages.add_message(request, messages.ERROR, str(err), extra_tags='danger')

    return redirect("news:news_by_category", slug=slug)


@csrf_exempt
def news_export(request, category_id=None):
    response = {"status": 200, "file": None}
    try:
        if request.headers.get('X-Requested-With') == 'XMLHttpRequest' and request.method == "POST":
            url = handle_news_export(category_id)
            if url:
                response["file"] = url
        else:
            raise Exception("Not an ajax request!")
    except Exception as err:
        response["status"] = 400
        response["message"] = str(err)
    return JsonResponse(response, status=response["status"])
