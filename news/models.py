from django.contrib.auth.models import User
from django.db import models
from django.utils.text import slugify
from django.core.exceptions import ValidationError

from uuid import uuid4


# Create your models here.
class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Categories(Base):
    slug = models.SlugField(blank=True, null=True, max_length=100)
    name = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        if Categories.objects.filter(name__icontains=self.name).exists():
            raise ValidationError("Category already exist!")
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class News(Base):
    category = models.ForeignKey(Categories, on_delete=models.CASCADE)

    sentence = models.TextField()
    label = models.CharField(max_length=50)
    score = models.FloatField()
    transformers_result_dict = models.JSONField()  # for debugging purpose


def get_news_export_upload_to(instance, filename):
    category_name = "All" if not instance.category else str(instance.category.name).capitalize()
    return f'NewsExport/{category_name}/{filename}'


class NewsExports(Base):
    category = models.ForeignKey(Categories, on_delete=models.CASCADE, blank=True, null=True)
    file = models.FileField(upload_to=get_news_export_upload_to)
