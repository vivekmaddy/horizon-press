from django.contrib import admin
from .models import *


# Register your models here.
class CategoriesAdmin(admin.ModelAdmin):
    list_display = ("id", "slug", "name")


class NewsAdmin(admin.ModelAdmin):
    list_display = ("id", "category", "label", "score", "sentence")


class NewsExportsAdmin(admin.ModelAdmin):
    list_display = ("id", "category", "file",)


admin.site.register(Categories, CategoriesAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(NewsExports, NewsExportsAdmin)
