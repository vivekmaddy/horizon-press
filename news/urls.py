from django.urls import path
from news import views

app_name = "news"
urlpatterns = [
    path("", views.horizon_press_home),
    path("<str:slug>", views.horizon_press_home, name="news_by_category"),
    path("submit_news/<str:slug>", views.submit_news, name="submit_news"),

    path('news_export/<int:category_id>/', views.news_export, name="news_export"),
    path('news_export/', views.news_export, name="news_export"),
]
