# Generated by Django 5.0 on 2024-01-01 10:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0006_alter_news_label'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='label',
            field=models.CharField(default=1, max_length=50),
            preserve_default=False,
        ),
    ]
