# Generated by Django 5.0 on 2024-01-01 13:49

import django.db.models.deletion
import news.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0007_alter_news_label'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsExports',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('file', models.FileField(upload_to=news.models.get_news_export_upload_to)),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='news.categories')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
