from django.forms.models import model_to_dict

from transformers import pipeline

from datetime import datetime

from news.models import NewsExports, News, Categories

import pandas as pd
import os

now = datetime.now()


def get_sentence_sentiment(sentence):
    try:
        classifier = pipeline(task="sentiment-analysis")
        preds = classifier(sentence)
        if not preds:
            raise Exception("Unable to find sentiment-analysis")
        preds = [{"score": round(pred["score"], 4), "label": pred["label"]} for pred in preds]

        return True, preds[0], ""
    except Exception as err:
        return False, None, str(err)


def handle_news_export(category: int):
    data = {
        "Serial No.": [],
        "Review": [],
        "Sentiment": []
    }
    news = News.objects.order_by("-id")
    category_name = ""

    if category:
        category = Categories.objects.get(id=category)
        category_name = f"{category.name}_"
        news = news.filter(category=category)
    else:
        data["Category"] = list(news.values_list("category__name", flat=True))

    data["Serial No."] = [i for i in range(1, news.count() + 1)]
    data["Review"] = list(news.values_list("sentence", flat=True))
    data["Sentiment"] = list(news.values_list("label", flat=True))

    df = pd.DataFrame(data)
    now_str = now.strftime("%d_%m_%Y_%I_%M_%p")
    file_name = f'HorizonPress_{category_name.upper()}{now_str}.csv'
    df.to_csv(file_name, index=False)

    export_instance = NewsExports()
    export_instance.category = category
    export_instance.file.save(file_name, open(file_name, 'rb'))

    if os.path.exists(file_name):
        os.remove(file_name)

    return export_instance.file.url
